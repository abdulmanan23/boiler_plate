import firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyAfXkfQRQ0cmgNuosA9cPYb9Qb_1ZwFWLg",
    authDomain: "react-boilerplate-c4bb4.firebaseapp.com",
    databaseURL: "https://react-boilerplate-c4bb4-default-rtdb.firebaseio.com",
    projectId: "react-boilerplate-c4bb4",
    storageBucket: "react-boilerplate-c4bb4.appspot.com",
    messagingSenderId: "563480065918",
    appId: "1:563480065918:web:6b0ae286c39eb9cba8252f",
    measurementId: "G-HSKNWYDYHV"
};

firebase.initializeApp(firebaseConfig);

export default firebase;
