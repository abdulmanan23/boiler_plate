import React from "react";
import "./App.css";
import DashboardRoutes from "./routes/dashboardRoutes/DashboardRoutes";
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Login from "./components/login/Login";
import SignUp from "./components/signup/SignUp";
import { ThemeProvider } from "styled-components";
import { lightTheme, darkTheme, GlobalStyle } from "./themes/themes";
import { useSelector } from "react-redux";


function App() {
  const { theme } = useSelector(state => state.postReducer);


  return (
    <Router>
      <Switch>
        <Route exact path="/login" component={Login} />
        <Route exact path="/signup" component={SignUp} />
        <ThemeProvider theme={theme === 'light' ? lightTheme : darkTheme}>
          <GlobalStyle />
          <DashboardRoutes />
        </ThemeProvider>
      </Switch>
    </Router >
  );
}

export default App;
