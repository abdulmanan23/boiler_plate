
import axios from "axios";
import { toast } from "react-toastify";
import { NETWORK_ERROR } from "../constants/constant";

const axiosInstance = axios.create({
    baseURL: process.env.REACT_APP_URL
})

//interceptor for response
function errorResponseHandler(error) {
    if (!error.response) {
        toast.error(NETWORK_ERROR)
    }
    if (error.response && error.response.status === 401) {
        localStorage.clear();
        // history.push("/login");
    }
}

// apply interceptor on response

//Request interceptor that willrun every time before each API call.
// axiosInstance.interceptors.request.use((config) => {
//     console.log("request interceptor", config)
//     config.headers.genericKey = "someGenericValue";
//     return config;
// }, (error) => {
//     console.log("error=", error)
//     return Promise.reject(error);
// });

axiosInstance.interceptors.response.use(
    response => response,
    errorResponseHandler
);
export default axiosInstance;