import React, { useState, useEffect } from "react";
import { Form, Card } from "react-bootstrap";
import { useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import firebase from "../../base/base";
import { isEmpty } from "../../utils/utils";


const CreateToDo = () => {

  const [data, setData] = useState({
    name: "",
    roll: "",
    section: "",
    isPresent: "",
  });
  const { id } = useParams();
  const { theme } = useSelector(state => state.postReducer);
  const [userList, setUserList] = useState([]);
  const [isClicked,setIsClicked]=useState(false);
  const [isSubmitted,setIsSubmiited]=useState(false)
  const [activeUserId,setActiveUserId]=useState('');

  useEffect(()=>{
   if(isClicked)
    {
      //scrool into bottom
      let targetElement = document.getElementById("card");
        targetElement.scrollIntoView();
        setIsSubmiited(false)
    }
  
  },[isClicked])



  useEffect(()=>{
    if(isSubmitted)
     {
       //scrool into top
       let targetElement = document.getElementById("section");
         targetElement.scrollIntoView();
         setIsClicked(false)
     }
   
   },[isSubmitted])
 

  useEffect(() => {
    const userRef = firebase.database().ref("user");
    userRef.on("value", snapshot => {
      const user = snapshot.val();
      const userList = [];
      for (let id in user) {
        userList.push({ id, ...user[id] });
      }
      setUserList(userList);
    });
  }, []);

  const handleChange = event => {
    setData({ ...data, [event.target.name]: event.target.value });
  };

  const createUser = (e) => {
    e.preventDefault();
    setIsSubmiited(true)
    const user = firebase.database().ref("user");
    user.push(data);
    clearData();
  };

  const deleteUser = id => {
    const userRef = firebase.database().ref("user").child(id);
    userRef.remove();
  };

  const getUser = (user) => {
    setActiveUserId(user.id)
    setIsClicked(true)
    setData({...data,
      name:user.name,
      section: user.section,
      roll: user.roll,
      isPresent: user.isPresent
    });
  };


  const updateUser = (e) => {
    e.preventDefault();
    const userRef = firebase.database().ref("user").child(activeUserId);
    userRef.update({
      name: data.name,
      roll: data.roll,
      section: data.section,
      isPresent: data.isPresent,
    });
    clearData();
    setIsSubmiited(true)
    setActiveUserId('');

  };

  const clearData = () => {
    setData({
      ...data,
      name: "",
      section: "",
      roll: "",
      isPresent: "",
    });
  };

  return (
    <>
      <div className="text-container">
        <div className="row" id="section">
              {userList.map(user => {
                return (
                  <div className="col-xl-3 col-lg-4 col-md-6 col-sm-12 mt-3" >
                  <Card
                    className={`card-corners ${
                      theme === "dark" && "dark-card"
                    }`}
                  >
                    <Card.Title className="text-center mt-3" >
                      User Details
                    </Card.Title>
                    <Card.Body>
                      <Card.Text>
                        <b>Full Name:</b>
                        {user.name}
                      </Card.Text>
                      <Card.Text>
                        {" "}
                        <b>Roll:</b>
                        {user.roll}
                      </Card.Text>
                      <Card.Text>
                        {" "}
                        <b>Section:</b>
                        {user.section}
                      </Card.Text>
                      <Card.Text>
                        {" "}
                        <b>Available:</b>
                        {user.isPresent}
                      </Card.Text>
                        <button
                          className={`btn-style border-radius ${
                            theme === "light"
                              ? "btn btn-primary"
                              : "dark-button"
                          }`}
                          type="submit"
                          onClick={(event) => getUser(user)}
                        >
                          Update
                        </button>
                      <button
                        type="submit"
                        className={`ml-2 btn-style border-radius ${
                          theme === "light" ? "btn btn-danger" : "dark-button"
                        }`}
                        onClick={() => deleteUser(user.id)}
                      >
                        Delete
                      </button>
                    </Card.Body>
                  </Card>
                  </div>
                );
              })}
        </div>
        <div className="row mt-4">
          <div className="col-lg-3 col-md-3 col-sm-2"></div>
          <div className="col-lg-6 col-md-6 col-sm-8">
            <Form>
              <>
                <Card
                  className={`card-corners ${theme === "dark" && "dark-card"}`}
                  id="card"
                >
                  {isEmpty(id) ? (
                    <Card.Title className="text-center mt-4">
                      Create User{" "}
                    </Card.Title>
                  ) : (
                    <Card.Title className="text-center mt-4">
                      Edit User{" "}
                    </Card.Title>
                  )}
                  <Card.Body>
                    <Form.Group controlId="formBasicEmail">
                      <Form.Label>Name</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Enter Full Name"
                        onChange={handleChange}
                        autoComplete="off"
                        name="name"
                        value={data.name}
                      />
                    </Form.Group>
                    <Form.Group controlId="formBasicEmail">
                      <Form.Label>Roll #</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Enter Roll #"
                        onChange={handleChange}
                        autoComplete="off"
                        name="roll"
                        value={data.roll}
                      />
                    </Form.Group>
                    <Form.Group controlId="formBasicEmail">
                      <Form.Label>Section</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Enter section"
                        onChange={handleChange}
                        autoComplete="off"
                        name="section"
                        value={data.section}
                      />
                    </Form.Group>
                    <Form.Group controlId="formBasicEmail">
                      <Form.Label>Present</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Enter "
                        onChange={handleChange}
                        autoComplete="off"
                        name="isPresent"
                        value={data.isPresent}
                      />
                    </Form.Group>
                    <div className="mt-3">
                      {/* {isLoading ? (
                        <ButtonLoader color="info" />
                      ) : ( */}
                      {!activeUserId ? (
                        <button
                          className={`ml-2 btn-style border-radius ${
                            theme === "light"
                              ? "btn btn-primary"
                              : "dark-button"
                          }`}
                          onClick={createUser}
                        >
                          Submit
                        </button>
                      ) : (
                        <button
                          className={`ml-2 btn-style border-radius ${
                            theme === "light"
                              ? "btn btn-primary"
                              : "dark-button"
                          }`}
                          onClick={(event) => updateUser(event)}
                        >
                          Update
                        </button>
                      )}

                      {/* )} */}
                      <Link to="/">
                        <button
                          type="submit"
                          className={`ml-2 btn-style border-radius ${
                            theme === "light"
                              ? "btn btn-primary"
                              : "dark-button"
                          }`}
                        >
                          Cancel
                        </button>
                      </Link>
                    </div>
                  </Card.Body>
                </Card>
              </>

              {/* )} */}
            </Form>
          </div>
        </div>
      </div>
    </>
  );
};
export default CreateToDo;
