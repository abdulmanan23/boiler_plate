import React, { useEffect, useState, useContext } from "react";
import DataTable, { createTheme } from "react-data-table-component";
import { Button, Card, Dropdown, Modal } from "react-bootstrap";
import {
  getAllPosts,
  deletePost,
  clearPosts,
} from "../../redux/actions/postActions";
import { startLoader } from "../../redux/actions/loaderActions";
import ButtonLoader from "../common/ButtonLoader";
import { useDispatch, useSelector } from "react-redux";
import "./postList.scss";
import moment from "moment";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
// import jwtDecode from "jwt-decode";
import Spinner from "../common/Spinner";
import UserContext from "../common/userContext/UserContext";

createTheme("darkTheme", {
  text: {
    primary: "#fff",
    secondary: "#fff",
  },
  background: {
    default: "#25282c",
  },
  divider: {
    default: "#101010",
  },
  action: {
    button: "rgba(0,0,0,.54)",
    hover: "rgba(0,0,0,.08)",
    disabled: "rgba(0,0,0,.12)",
  },
});

const PostList = () => {
  const history = useHistory();
  const user = useContext(UserContext);

  const {
    postReducer: posts,
    postReducer: { theme },
  } = useSelector(state => state);

  const isLoading = useSelector(({ loaderReducer }) => loaderReducer.isLoading);
  const [showModal, setShow] = useState(false);
  const [rows, setRows] = useState([]);
  if (localStorage.getItem("userToken")) {
    // const token = localStorage?.getItem("userToken");
    // var decoded = jwtDecode(token);
    // var LoggedInUserId = decoded.id;
  }

  const handleClose = () => {
    setShow(false);
  };
  // const handleShow = id => {
  //   setShow(true);
  //   setActivePost(id);
  // };

  const viewHandler = id => {
    history.push(`view/post/${id}`);
  };
  const handleDelete = id => {
    dispatch(startLoader());
    dispatch(deletePost(id, history));
    setTimeout(() => {
      setShow(false);
    }, 2000);
  };
  const columns = [
    {
      name: "title",
      selector: "title",

      minWidth: "10rem",

      center: true,
    },
    {
      name: "Description",
      selector: "body",
      minWidth: "20rem",
      center: true,
    },
    {
      name: "Date",
      selector: d => {
        return moment(d.date).format("MMMM Do YYYY, h:mm:ss a");
      },
      minWidth: "10rem",
      sortable: true,
      center: true,
    },
    {
      name: "Time",
      selector: d => {
        return moment(d.date).format("h:mm:ss");
      },
      minWidth: "4rem",
      sortable: true,
      center: true,
    },
    {
      name: "Actions",
      minWidth: "20rem",
      cell: post => (
        <Dropdown>
          <Dropdown.Toggle variant="success" id="dropdown-basic">
            Actions
          </Dropdown.Toggle>
          <Dropdown.Menu>
            <Dropdown.Item onClick={() => viewHandler(post.id)}>
              View
            </Dropdown.Item>
            {/* {localStorage.getItem("userToken") &&
              LoggedInUserId === post.user && ( */}
            <Dropdown.Item onClick={() => handleDelete(post.id)}>
              Delete
            </Dropdown.Item>
            {/* )} */}
          </Dropdown.Menu>
        </Dropdown>
      ),
      ignoreRowClick: true,
      allowOverflow: true,
      button: true,
      center: true,
    },
  ];
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(startLoader());
    dispatch(getAllPosts());
  }, [dispatch]);

  function dragStart(event) {
    event.dataTransfer.setData("text", `${event.target.id},${event.clientY}`);
    event.target.parentElement.id = "parent";
  }
  function dragOver(event) {
    event.preventDefault();
    event.dataTransfer.setData("text", event.target.id);
  }
  function dragEnd(event) {
    const [sourceId, Y] = event.dataTransfer.getData("text").split(",");
    event.preventDefault();
    const row = event.target.parentNode;
    const parent = row.parentNode;
    const sourceNodeId = sourceId;
    const sourceNode = document.getElementById(sourceNodeId);
    const destinationNode = event.target.parentNode;
    if (parent === sourceNode.parentElement) {
      if (Y > event.clientY) parent.insertBefore(sourceNode, destinationNode);
      else parent.insertBefore(sourceNode, destinationNode.nextSibling);
    }
  }

  useEffect(() => {
    const allRows = document.getElementsByClassName("dmCPBb");
    setRows(allRows);
    if (rows && rows?.length > 0) {
      for (let row of rows) {
        row.draggable = true;
      }
    }
  }, [rows]);

  useEffect(() => {
    return () => {
      dispatch(clearPosts());
    };
  }, [dispatch]);

  useEffect(() => {
    document.addEventListener("dragstart", dragStart);
    document.addEventListener("dragover", dragOver);
    document.addEventListener("drop", dragEnd);
    return () => {
      document.removeEventListener("dragStart", dragStart);
      document.removeEventListener("dragend", dragEnd);
      document.removeEventListener("dragover", dragOver);
    };
  }, []);

  return (
    <>
      <Modal show={showModal} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Post Deletion</Modal.Title>
        </Modal.Header>
        <Modal.Body>Are you sure You want to Delete the Post?</Modal.Body>
        <Modal.Footer>
          {isLoading ? (
            <ButtonLoader color="success" />
          ) : (
            <Button
              variant="success"
              className="btn-style border-radius"
              onClick={() => handleDelete()}
            >
              Confirm
            </Button>
          )}

          <Button
            variant="danger"
            className="border-radius"
            onClick={handleClose}
          >
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>
      {/* <Header /> */}
      <div className="containers">
        <div className="row">
          <div className="col-sm-2"></div>
          <div className="col-sm-8">
            <div className={"table-conatiner"}>
              <div>
                <Link to="/create/todo">
                  <button
                    className={
                      theme === "light"
                        ? "btn btn-primary ml-3"
                        : "dark-button ml-3"
                    }
                    style={{ width: "140px" }}
                  >
                  Create User{" "}
                  </button>
                </Link>
              </div>
              <h6 className="text-center"> Welcome {user?.fullName}</h6>
              {/* <LineCharts /> */}
              <Card
                className={`card-corners padding ${
                  theme === "dark" && "dark-card"
                }`}
              >
                <Card.Title className="text-center mt-3">Post Lists</Card.Title>
                <Card.Body>
                  <div className="ml-auto mb-3">
                    <Link to="/create/post">
                      <button
                        className={`
                  ${theme === "light" ? "btn btn-primary" : "dark-button"}
                  `}
                      >
                        Create Post{" "}
                      </button>
                    </Link>
                  </div>

                  {posts?.posts?.length === 0 && isLoading ? (
                    <div className="text-center">
                      <Spinner />
                    </div>
                  ) : (
                    <>
                      <DataTable
                        data={posts?.posts}
                        columns={columns}
                        header
                        theme={theme === "dark" && "dark"}
                        selectableRows
                        defaultSortAsc
                        pagination
                      />
                    </>
                  )}
                </Card.Body>
              </Card>
            </div>
          </div>
          <div className="col-sm-2"></div>
        </div>
      </div>
    </>
  );
};

export default PostList;
