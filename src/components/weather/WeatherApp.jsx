import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { API_KEY } from "../../constants/constant";
import { Card, Form } from "react-bootstrap";
import { startLoader, stopLoader } from "../../redux/actions/loaderActions";
import ButtonLoader from "../common/ButtonLoader";
import "./weather.scss";

function WeatherApp() {
  const key = API_KEY;

  const [data, setData] = useState({});
  const { theme } = useSelector(state => state.postReducer);
  const [formData, setForm] = useState({ location: "", days: "" });
  const [record, setRecord] = useState("");
  const { location, days } = formData;
  const { loaderReducer: isLoading } = useSelector(state => state);

  const dispatch = useDispatch();

  const [error, setError] = useState("");
  const handleChange = ({ target: input }) => {
    if (input.name === "days") {
      setRecord(input.value);
    }

    setForm({ ...formData, [input.name]: input.value });
  };
  const handleSearch = () => {
    if (formData.location !== "" && formData.days !== "") {
      dispatch(startLoader());
      fetch(
        `https://api.openweathermap.org/data/2.5/weather?q=${formData.location},&cnt=${formData.days}&APPID=` +
          key +
          "&units=metric"
      )
        .then(res => res.json())
        .then(data => {
          if (data.message === "city not found") {
            dispatch(stopLoader());
            setError(data.message);
            setTimeout(function () {
              setError("");
            }, 4000);
          } else {
            dispatch(stopLoader());
            setData(data);
            dispatch(stopLoader());
          }
          setForm({ location: "", days: "" });
        });
    }
  };
  const handleClear = () => {
    setData({});
    setForm({ location: "", days: "" });
  };
  return (
    <div className="weather-container">
      <div className="row">
        <div className="col-lg-4"></div>
        <div className="col-lg-4">
          {error && (
            <div className="text-center error-border">
              <span className="span-style">{error}</span>
            </div>
          )}

          <Card
            className={`card-corners padding ${
              theme === "dark" && "dark-card"
            }`}
          >
            <Card.Title>
              <h4 className="weather-head">Weather Application</h4>
            </Card.Title>
            <Card.Body>
              <Form>
                <Form.Group controlId="formBasicName">
                  <Form.Label>Country or City</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter Country or City"
                    name="location"
                    onChange={handleChange}
                    autoComplete="off"
                    value={location}
                  />
                </Form.Group>
                <Form.Group controlId="formBasicName">
                  <Form.Label>No. of Days</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter Country"
                    name="days"
                    onChange={handleChange}
                    autoComplete="off"
                    value={days}
                  />
                </Form.Group>
                <Form.Group controlId="formBasicName" className="text-center">
                  {isLoading?.isLoading ? (
                    <ButtonLoader />
                  ) : (
                    <button
                      className={`
                     ${theme === "light" ? "btn btn-primary" : "dark-button"}
                    `}
                      onClick={handleSearch}
                    >
                      Search
                    </button>
                  )}

                  {Object.entries(data).length > 0 && (
                    <button
                      className={` ml-2
                      ${theme === "light" ? "btn btn-danger" : "dark-button"}
                     `}
                      onClick={handleClear}
                    >
                      Clear
                    </button>
                  )}
                </Form.Group>
              </Form>
            </Card.Body>
          </Card>

          {Object.entries(data).length > 0 && (
            <Card
              className={`card-corners mt-4 ${theme === "dark" && "dark-card"}`}
            >
              <Card.Body className="text-center">
                <h2>Record of {record} days</h2>
                <h5>Location:{data?.name}</h5>
                <h5>Weather Description:{data?.weather[0]?.description}</h5>
                <h5>Temperature:{data?.main?.temp}°c</h5>
                <h5>Pressure: {data?.main?.pressure}</h5>
                <h5>Humidity:{data?.main?.humidity}%</h5>
                <h5>Wind Speed:{data?.wind?.speed}</h5>
                <h5>Wind Degree:{data?.wind?.deg}</h5>
              </Card.Body>
            </Card>
          )}
        </div>
        <div className="col-lg-4"></div>
      </div>
    </div>
  );
}
export default WeatherApp;
