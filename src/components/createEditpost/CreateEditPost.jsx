import React, { useState, useEffect, useRef, useContext } from "react";
import { Form, Button, Card } from "react-bootstrap";
import "./createEditPost.scss";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { createPost } from "../../redux/actions/postActions";
import { getPost } from "../../redux/actions/postActions";
import { Link } from "react-router-dom";
import { toast, Zoom } from "react-toastify";
import { useRouteMatch } from "react-router-dom";
import { REQUIRED_FIELDS_ERROR } from "../../constants/constant";
import { startLoader } from "../../redux/actions/loaderActions";
import ButtonLoader from "../../components/common/ButtonLoader";
import moment from "moment";
import UserContext from "../common/userContext/UserContext";

export default function CreateEditPost() {
  const {
    postReducer: post,
    postReducer: { theme },
  } = useSelector(state => state);
  const user = useContext(UserContext);
  const isLoading = useSelector(({ loaderReducer }) => loaderReducer.isLoading);
  const [errors, setErrors] = useState({ postText: "" });
  const [postText, setPost] = useState("");
  const history = useHistory();
  const dispatch = useDispatch();
  const { params } = useRouteMatch();
  const postId = params.id;

  useEffect(() => {
    if (postId) {
      dispatch(getPost(postId));
    }
  }, [dispatch, postId]);

  const handleChange = ({ target: input }) => {
    setPost(input.value);
    let error;
    switch (input.name) {
      case "postText":
        error =
          input.value.length < 10
            ? "Text must be between 10 or 300 Characters Long"
            : "";
        setErrors({ ...errors, [input.name]: error });
        break;
      default:
        break;
    }
  };
  const validateForm = errors => {
    let valid = true;
    Object.values(errors).forEach(
      // if we have an error string set valid to false
      val => val.length > 0 && (valid = false)
    );
    return valid;
  };

  const handlePost = event => {
    event.preventDefault();
    if (validateForm(errors)) {
      if (postText !== "") {
        var postObject = {
          title: postText,
        };
        dispatch(startLoader());
        dispatch(createPost(postObject, history));
      } else {
        toast.warning(REQUIRED_FIELDS_ERROR);
      }
    }
  };
  toast.configure({
    position: "top-center",
    autoClose: 3000,
    transition: Zoom,
  });
  const prevName = useRef("");

  useEffect(() => {
    prevName.current = postText;
  });

  return (
    <>
      <div className="text-container">
        <div className="row">
          <div className="col-lg-3 col-md-3 col-sm-2"></div>
          <div className="col-lg-6 col-md-6 col-sm-8">
            <Form>
              {!postId && (
                <>
                  <Card
                    className={`card-corners mt-4 ${
                      theme === "dark" && "dark-card"
                    }`}
                  >
                    <Card.Title className="text-center">
                      Create Post{" "}
                    </Card.Title>
                    <Card.Body>
                      <Form.Group controlId="formBasicEmail">
                        <Form.Label>Post</Form.Label>
                        <Form.Control
                          ref={prevName}
                          type="text"
                          placeholder="Enter post"
                          onChange={handleChange}
                          autoComplete="off"
                          name="postText"
                        />

                        <Form.Text className="text-muted">
                          <Form.Text>
                            {errors.postText.length > 0 && (
                              <span className="error">{errors.postText}</span>
                            )}
                          </Form.Text>
                        </Form.Text>
                      </Form.Group>
                      <div className="mt-3">
                        {isLoading ? (
                          <ButtonLoader color="info" />
                        ) : (
                          <button
                            className={`ml-2 border-radius
                            ${
                              theme === "light"
                                ? "btn btn-primary"
                                : "dark-button"
                            }
                            `}
                            type="submit"
                            onClick={handlePost}
                          >
                            Submit
                          </button>
                        )}
                        <Link to="/">
                          <button
                            className={`ml-2 border-radius
                            ${
                              theme === "light"
                                ? "btn btn-danger"
                                : "dark-button"
                            }
                            `}
                          >
                            Cancel
                          </button>
                        </Link>
                      </div>
                    </Card.Body>
                  </Card>
                </>
              )}

              {postId && (
                <Card
                  className={`card-corners mt-4 ${
                    theme === "dark" && "dark-card"
                  }`}
                >
                  <Card.Title className="text-center mt-3">
                    Post Details
                  </Card.Title>
                  <Card.Body>
                    <Card.Text>
                      {" "}
                      <b>Title:</b>
                      {post?.post?.title}
                    </Card.Text>
                    <Card.Text>
                      <b>User Id:</b>
                      {post?.post?.userId}
                    </Card.Text>
                    <Card.Text>
                      {" "}
                      <b>Date:</b>
                      {moment(post?.post?.date).format("MMMM DD YYYY")}
                    </Card.Text>
                    <Link to="/">
                      <Button
                        className={`ml-auto border-radius
                        ${theme === "light" ? "btn btn-primary" : "dark-button"}
                        `}
                      >
                        Cancel
                      </Button>
                    </Link>
                  </Card.Body>
                </Card>
              )}
            </Form>
          </div>
          <div className="col-lg-3 col-md-3 col-sm-2">
            <Card
              className={`card-corners mt-4 ${theme === "dark" && "dark-card"}`}
            >
              <Card.Title className="text-center mt-3">User Details</Card.Title>
              <Card.Body>
                <Card.Text>
                  {" "}
                  <b>Full Name:</b>
                  {user.fullName}
                </Card.Text>
                <Card.Text>
                  <b>Age:</b>
                  {user.age}
                </Card.Text>
                <Card.Text>
                  {" "}
                  <b>Batch:</b>
                  {user.batch}
                </Card.Text>
                <Card.Text>
                  {" "}
                  <b>Phone:</b>
                  {user.contact}
                </Card.Text>{" "}
                <Card.Text>
                  {" "}
                  <b>Email:</b>
                  {user.email}
                </Card.Text>
                <Card.Text>
                  {" "}
                  <b>Fees:</b>
                  {user.isPaid ? <span>Paid</span> : <span>Unpaid</span>}
                </Card.Text>
              </Card.Body>
            </Card>
          </div>
        </div>
      </div>
    </>
  );
}
