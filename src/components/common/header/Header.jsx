import React from "react";
import { Nav, Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";
import "./header.scss";
import { useHistory } from "react-router-dom";
import ThemeToggler from "../toggler/Toggler";
import { useSelector } from "react-redux";

export default function Header() {
  const { theme } = useSelector(state => state.postReducer);
  const history = useHistory();
  const logoutHandler = () => {
    localStorage.clear();
    history.push("/login");
  };
  return (
    <div>
      <div>
        <Navbar
          collapseOnSelect
          expand="md"
          className={theme === "light" ? "nav-bg" : "darkTheme nav-border"}
        >
          <Navbar.Brand to="#home">
            <img
              src="/assets/images/LogoIconGold.png"
              className="logo-style"
              alt="React Bootstrap logo"
            />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <Link to="/" className="link-style">
                Home
              </Link>
              <Link to="/create/profile" className="link-style">
                Profile
              </Link>
              <Link to="/view/map" className="link-style">
                Map
              </Link>
              <Link to="/calculator" className="link-style">
                Calculator
              </Link>
              <Link to="/drag" className="link-style">
                Drag
              </Link>
              <Link to="/web/camera" className="link-style">
                Camera
              </Link>
              <Link to="/weather" className="link-style">
                Weather
              </Link>
            </Nav>
            <Nav>
              <ThemeToggler />
            </Nav>

            <Nav>
              <Link to="/login">
                <button
                  type="button"
                  className={`
                  ${theme === "light" ? "btn btn-primary" : "dark-button"}
                  `}
                  onClick={logoutHandler}
                >
                  Log Out
                </button>
              </Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>
    </div>
  );
}
