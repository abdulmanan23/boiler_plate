import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { updateTheme } from "../../../redux/actions/postActions";

const ThemeToggler = () => {
  const [theme, setTheme] = useState(localStorage.getItem("theme"));
  const dispatch = useDispatch();

  const switchTheme = () => {
    theme === "light" ? setTheme("dark") : setTheme("light");
  };

  useEffect(() => {
    localStorage.setItem("theme", theme);
    dispatch(updateTheme(theme));
  }, [dispatch, theme]);

  return (
    <div className="form-check form-switch d-flex ">
      <div>
        <label
          className="form-check-label"
          for="flexSwitchCheckChecked"
          style={{ color: "white" }}
        >
          Enable Dark Mode
        </label>
      </div>
      <div className="ml-5 mr-5">
        <input
          className="form-check-input"
          type="checkbox"
          id="flexSwitchCheckChecked"
          onClick={switchTheme}
          checked={theme === "dark"}
        />
      </div>
    </div>
  );
};
export default ThemeToggler;
