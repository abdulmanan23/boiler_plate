import { createContext } from "react";

const UserContext = createContext({
  fullName: "Abdul Manan",
  age: 23,
  isPresnt: true,
  isPaid: true,
  batch: "2015-19",
  contact: "03234314270",
  email: "abdulmanan222111@gmail.com",
});
export default UserContext;
