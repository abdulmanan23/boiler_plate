import React from "react";
import { Spinner } from "react-bootstrap";
import { useSelector } from "react-redux";

export default function ButtonLoader() {
  const { theme } = useSelector(state => state.postReducer);
  return (
    <>
      <button
        button
        className={`btn-style border-radius
      ${theme === "light" ? "btn btn-primary" : "dark-button"}
      `}
      >
        <Spinner
          as="span"
          animation="border"
          size="sm"
          role="status"
          aria-hidden="true"
        />
        <span style={{ paddingLeft: "1px" }}>Loading...</span>
      </button>{" "}
    </>
  );
}
