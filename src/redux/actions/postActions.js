import { toast } from "react-toastify";
import {
  POST_SUCCESS,
  POST_DELETE_MESSAGE,
} from "../../constants/constant";
import {
  GET_ALL_POSTS,
  CREATE_POST,
  GET_POST,
  CLEAR_POSTS,
  SET_THEME
} from "../../constants/actionTypes";
import { stopLoader } from "../actions/loaderActions";
import axiosInstance from "../../interceptor/interceptor";

export const getAllPosts = () => (dispatch) => {
  axiosInstance
    .get("/posts")
    .then((res) => {
      dispatch(stopLoader());
      dispatch({
        type: GET_ALL_POSTS,
        payload: res?.data,
      });
    })
};

export const createPost = (object, history) => (dispatch) => {

  axiosInstance
    .post("/posts", object)
    .then((res) => {

      dispatch(stopLoader());
      if (res) {
        toast.success(POST_SUCCESS);
        history.push("/");
        dispatch({
          type: CREATE_POST,
          payload: res?.data,
        });
      }
    })
};

export const getPost = (id, history) => (dispatch) => {
  axiosInstance
    .get(`/posts/${id}`)
    .then((res) => {
      dispatch({
        type: GET_POST,
        payload: res?.data,
      });
    })
};
export const updateTheme = (theme) => (dispatch) => {

  const currentTheme = localStorage.getItem("theme");
  dispatch({
    type: SET_THEME,
    payload: currentTheme,
  });

};

export const deletePost = (id, history) => (dispatch) => {
  axiosInstance
    .delete(`/posts/${id}`)
    .then(() => {
      dispatch(stopLoader());
      toast.success(POST_DELETE_MESSAGE);
      // dispatch(getAllPosts());
    });
};
export const clearPosts = () => {
  return { type: CLEAR_POSTS };
};
